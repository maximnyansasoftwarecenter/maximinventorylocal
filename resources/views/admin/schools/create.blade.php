@extends('layouts.base')

@push('css')

@endpush


@section('title','Items')


@section('content')
    @include('partials.alert')
    <div class="container-fluid" style="padding-bottom: 4rem;">
        <div class="top-flex-box">
            <p><span style="font-size: 4rem; " class="fa fa-graduation-cap"></span></p>
            <h2 style="font-weight: bold;">Add School</h2>
        </div>

        <div class="row">
            <br>
            <br>
            <div class="row">
                <form method="post" action="{{route('schools.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-8 col-xs-10 col-md-offset-2 col-xs-offset-1" >
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{$errors->has('name')? 'has-error':''}}">
                                    <label class="control-label" for="">School Name</label>
                                    <input class="form-control" type="text" name="name">
                                    @if($errors->has('name'))
                                        <span class="help-block">{{$errors->first('name')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{$errors->has('address')? 'has-error':''}}">
                                    <label class="control-label" for="">Address</label>
                                    <input type="text" name="address" class="form-control" id="address">
                                    @if($errors->has('address'))
                                        <span class="help-block">{{$errors->first('address')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{$errors->has('location')? 'has-error':''}}">
                                    <label class="control-label " for="">Location</label>
                                    <input class="form-control" type="text" name="location">
                                    @if($errors->has('location'))
                                        <span class="help-block">{{$errors->first('location')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{$errors->has('region')? 'has-error':''}}">
                                    <label class="control-label " for="">Region</label>
                                    <select name="region" class="form-control">
                                        <option></option>
                                    </select>
                                    @if($errors->has('region'))
                                        <span class="help-block">{{$errors->first('region')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">District</label>
                                    <select class="form-control" name="district">

                                    </select>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" value="{{Auth::user()->id}}" name="id">
                        <button type="submit" class="btn btn-primary pull-right">Add School</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection


@push('scripts')
@endpush