@extends('layouts.base')

@push('css')

@endpush


@section('title','Items')


@section('content')
    @include('partials.alert')
    <div class="container-fluid" style="padding-bottom: 4rem;">
        <div class="top-flex-box">
            <p><span style="font-size: 4rem; " class="fa fa-globe"></span></p>
            <h2 style="font-weight: bold;">Add Regions & Districts</h2>
        </div>

        <div class="row">
            <br>
            <br>
            <div class="row">
                <form method="post" action="{{route('regions.store')}}">
                    @csrf
                        <div class="col-md-8 col-md-offset-2">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="" class="control-label">Region</label>
                                        <select  class="form-control chosen-select"  name="region" id="" data-placeholder="Choose a Region...">
                                            <option value="ashanti">Ashanti</option>
                                            <option value="western">Western</option>
                                            <option value="greater_accra">Greater Accra</option>
                                            <option value="central">Central</option>
                                            <option value="eastern">Eastern</option>
                                            <option value="northern">Northern Region</option>
                                            <option value="upper_east">Upper East</option>
                                            <option value="upper_west">Upper West</option>
                                            <option value="volta">Volta</option>
                                            <option value="brong_ahafo">Brong-Ahafo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="" class="control-label ">District</label>
                                    <select class="form-control chosen-select" name="" id="">
                                        @foreach($districts as $district)
                                            <option>{{$district->name}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>

                                <button class="btn btn-primary pull-right">Add</button>
                        </div>
                </form>
            </div>
        </div>
    </div>

@endsection


@push('scripts')
    <script type="text/javascript">
        $(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
    </script>
@endpush