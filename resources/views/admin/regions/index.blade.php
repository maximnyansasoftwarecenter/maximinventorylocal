@extends('layouts.base')

@push('css')

@endpush


@section('title','users')


@section('content')
    @include('partials.alert')
    <div class="container-fluid">
        <div class="top-flex-box">
            <p><span style="font-size: 4.5rem; " class="fa fa-globe"></span></p>
            <h2 style="font-weight: bolder;">Regions & Disricts</h2>
            <a href="{{ route('regions.create') }}" class="btn btn-primary icon__plus">Add New<span class=" fa fa-plus"></span></a>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table class="datatable table table-bordered table-striped table-responsive">
                    <thead>

                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Location</th>
                        <th>Avatar</th>
                        <th>Phone</th>
                        <th>User Type</th>
                        <th></th>
                    </tr>

                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>



@endsection


@push('scripts')

@endpush