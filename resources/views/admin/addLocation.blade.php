@extends('layouts.base')

@push('css')

@endpush


@section('title','Locations')


@section('content')
    @include('partials.alert')
    <div class="container-fluid" style="padding-bottom: 4rem;">
        <div class="top-flex-box">
            <p><span style="font-size: 4rem; " class="fa fa-location-arrow"></span></p>
            <h2 style="font-weight: bold;">Add Location</h2>
        </div>

        <div class="row">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">

                    <div style="max-height: 500px; " class="form-group {{$errors->has('location')? 'has-error':''}}">



                        {!! $maps['html'] !!}

                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <form method="post" action="{{route('locations.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group {{$errors->has('name')? 'has-error':''}}">
                            <label class="control-label" for="">Name</label>
                            <input class="form-control" type="text" name="name">
                            @if($errors->has('name'))
                                <span class="help-block">{{$errors->first('name')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('opening_hours')? 'has-error':''}}">
                            <label class="control-label " for="">Working Hours</label>
                            <input class="form-control" type="text" name="opening_hours">
                            @if($errors->has('opening_hours'))
                                <span class="help-block">{{$errors->first('opening_hours')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('image')? 'has-error':''}}">
                            <label class="control-label" for="">Avatar</label>
                            <input class="form-control" type="file" name="image">
                            @if($errors->has('image'))
                                <span class="help-block">{{$errors->first('image')}}</span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('location')? 'has-error':''}}">
                            <label class="control-label" for="">Location</label>
                            <select class="form-control" name="location" id="">
                                @foreach($locations as $location)
                                <option value="{{$location->id}}}">{{$location->address}}</option>
                                    @endforeach
                        </select>
                            @if($errors->has('location'))
                                <span class="help-block">{{$errors->first('location')}}</span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Location</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection


@push('scripts')
    {!! $maps['js'] !!}
@endpush