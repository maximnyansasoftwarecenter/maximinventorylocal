@extends('layouts.base')

@push('css')

@endpush


@section('title','')


@section('content')
    @include('partials.alert')
    <div class="container-fluid">
        <div class="top-flex-box">
            <p><span style="font-size: 4rem;" class="fa fa-user"></span></p>
            <h2 style="font-weight: bold;">Add User</h2>
        </div>
        <div class="row">
            <form method="post" action="{{route('user.store')}}" enctype="multipart/form-data">

                <div class="col-md-8 col-md-offset-2">
                    <div class="form-group {{$errors->has('name')? 'has-error':''}}">
                        <label class="control-label" for="">Name</label>
                        <input class="form-control" type="text" name="name">
                        @if($errors->has('name'))
                            <span class="help-block">{{$errors->first('name')}}</span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('phone')? 'has-error':''}}">
                        <label class="control-label" for="">Phone</label>
                        <input class="form-control" type="text" name="phone">
                        @if($errors->has('phone'))
                            <span class="help-block">{{$errors->first('phone')}}</span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('email')? 'has-error':''}}">
                        <label class="control-label " for="">Email</label>
                        <input class="form-control" type="text" name="email">
                        @if($errors->has('email'))
                            <span class="help-block">{{$errors->first('email')}}</span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('picture')? 'has-error':''}}">
                        <label class="control-label " for="">Avatar</label>
                        <input class="form-control" type="file" name="picture" />
                        @if($errors->has('picture'))
                            <span class="help-block">{{$errors->first('picture')}}</span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('user_type')? 'has-error':''}}">
                        <label class="control-label" for="">User Type</label>
                        <select class="form-control" name="user_type" id="">
                            <option value="super_admin">Super Admin</option>
                            <option value="collection_manager">Collections Manager</option>
                            <option value="maxim_agent">Maxim Nyansa Agent</option>
                        </select>
                        @if($errors->has('user_type'))
                            <span class="help-block">{{$errors->first('user_type')}}</span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('location')? 'has-error':''}}">
                        <label class="control-label" for="">Location</label>
                        <select class="form-control" name="location" id="">
                            @foreach($locations as $location)
                            <option value="{{$location->id}}">{{$location->name}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('location'))
                            <span class="help-block">{{$errors->first('location')}}</span>
                        @endif
                    </div>

                    <button type="submit" class="btn btn-primary pull-right">Add User</button>
                </div>
                @csrf
            </form>
        </div>
    </div>
@endsection


@push('scripts')

@endpush