@extends('layouts.base')

@push('css')

@endpush


@section('title','Locations')


@section('content')
    <div class="container-fluid">
        @include('partials.alert')
        <div class="container-fluid">
            <div class="top-flex-box">
                <p><span style="font-size: 4.5rem; " class="fa fa-location-arrow"></span></p>
                <h2 style="font-weight: bolder;">Locations</h2>
                <a href="{{route('admin.location.search')}}" class="btn btn-primary icon__plus">Add Location <span class=" fa fa-plus"></span></a>
            </div>
            <br>

            <br>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <table class="datatable table table-bordered table-striped table-responsive">
                        <thead>

                        <tr>
                            <th>Name</th>
                            <th>Opening Hours</th>
                            <th></th>
                        </tr>

                        </thead>
                        <tbody>
                        @foreach($locationsData as $loc)
                            <tr>
                                <td>{{$loc->name}}</td>
                                <td>{{$loc->opening_hours}}</td>
                                <td style="display: flex; justify-content: space-evenly;">
                                    <a type="button" class="btn btn-sm btn-primary admin-user-edit">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <button type="button" data-toggle="modal" data-target="#deleteModal{{$loc->id}}"  class="btn btn-danger btn-sm">
                                        <span class="fa fa-trash"></span>
                                    </button>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @foreach($locationsData as $loc)
        {{--this is the delete modal--}}

        <div id="deleteModal{{$loc->id}}" class="modal fade" tabindex="-1" role="dialog"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title text-center" style="font-weight: bolder;">Are You Sure?</h3>
                    </div>
                    <div class="modal-footer row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-md-6">
                            <form  action="{{route('locations.destroy',['id'=>$loc->id])}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-block">Confirm</button>
                            </form>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>

        {{-- this is the edit modal--}}

    @endforeach
@endsection


