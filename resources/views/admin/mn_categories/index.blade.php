@extends('layouts.base')

@push('css')

@endpush


@section('title','users')


@section('content')
    @include('partials.alert')
    <div class="container-fluid">
        <div class="top-flex-box">
            <p><span style="font-size: 4.5rem; " class="fa fa-list-alt"></span></p>
            <h2 style="font-weight: bolder;">Categories</h2>
            <a href="{{ route('mn_categories.create') }}" class="btn btn-primary icon__plus">Add Category <span class=" fa fa-plus"></span></a>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2 col-sm-offset-2">
                <table class="datatable table table-bordered table-striped table-responsive">
                    <thead>

                    <tr>
                        <th>Name</th>
                        <th></th>
                    </tr>

                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{$category->name}}</td>
                                <td style="display: flex; justify-content: space-evenly;">
                                    <a type="button"  data-id="{{$category->id}}"  class="btn btn-sm btn-primary category-edit">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <button data-id="{{$category->id}}" type="button" data-toggle="modal" data-target="#deleteModal{{$category->id}}"
                                            class="btn btn-danger btn-sm">
                                        <span class="fa fa-trash"></span>
                                    </button>
                                </td>
                            </tr>

                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @foreach($categories as $category)
        {{--this is the delete modal--}}
        <div id="deleteModal{{$category->id}}" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title text-center" style="font-weight: bolder;">Are You Sure?</h3>
                    </div>
                    <div class="modal-footer row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-md-6">
                            <form  action="{{route('mn_categories.destroy',['id'=>$category->id])}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-block">Confirm</button>
                            </form>
                        </div>

                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->

        </div>
        {{--edit modal--}}
        <div class="modal fade" :class="{ in: modalShown }"
             id="CategoryEditModal{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <form class="Category-Edit" action="{{route('mn_categories.update',['id'=>$category->id])}}" method="post" data-count="{{$errors->count()}}" data-errors={{$errors}}>
                    @csrf
                    @method("PUT")
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title text-center" style="font-weight: bolder;" id="myModalLabel">Edit Category</h3>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" class="form-control" name="id" value="{{$category->id}}">
                            <div class="form-group">
                                <label for="" class="control-label {{$errors->has('name')? 'has-error':''}}">Name</label>
                                <input type="text" class="form-control" id="name{{$category->id}}" name="name">
                                @if($errors->has('name'))
                                    <span class="help-block">{{$errors->first('name')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button"  class="pull-left btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit"  class="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach


@endsection


@push('scripts')
    <script type="text/javascript">
        var id;
        $('.category-edit').click(function(){
            id = $(this).data('id');
            console.log(id);
            $.ajax({
                type:'GET',
                url:'/admin/mn_categories/'+id+'/edit',
                success:(data)=>{
                    console.log(data);
                    $('#name'+id).val(data.name);

                },
                error:(error)=>{
                    e.preventDefault();
                    console.log(error);
                }
            });
            $('#CategoryEditModal'+id).modal('show');
        });


    </script>
@endpush