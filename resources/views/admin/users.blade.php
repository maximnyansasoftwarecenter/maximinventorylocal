@extends('layouts.base')

@push('css')

@endpush


@section('title','users')


@section('content')
    @include('partials.alert')
    <div class="container-fluid">
        <div class="top-flex-box">
            <p><span style="font-size: 4.5rem; " class="fa fa-user"></span></p>
            <h2 style="font-weight: bolder;">Users</h2>
            <a href="{{ route('admin.users.add') }}" class="btn btn-primary icon__plus">Create User <span class=" fa fa-plus"></span></a>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table class="datatable table table-bordered table-striped table-responsive">
                    <thead>

                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Location</th>
                            <th>Avatar</th>
                            <th>Phone</th>
                            <th>User Type</th>
                            <th></th>
                        </tr>

                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->location->name}}</td>
                        <td><img height="100" src="{{asset('/images/locations/'.$user->avatar)}}" alt="no images"></td>
                        <td>{{$user->phone}}</td>
                        <td>{{$user->user_type}}</td>
                        <td style="display: flex; justify-content: space-evenly;">
                            <a type="button"  data-id="{{$user->id}}"  class="btn btn-sm btn-primary admin-user-edit">
                                <span class="fa fa-pencil"></span>
                            </a>
                            <button data-id="{{$user->id}}" type="button" data-toggle="modal" data-target="#deleteModal{{$user->id}}"
                                    class="btn btn-danger btn-sm">
                                <span class="fa fa-trash"></span>
                            </button>
                        </td>
                        {{--this is the delete modal--}}
                        <div id="deleteModal{{$user->id}}" class="modal fade" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">

                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="modal-title text-center" style="font-weight: bolder;">Are You Sure?</h3>
                                    </div>

                                    <div class="modal-footer row">
                                        <div class="col-md-6">
                                            <button type="cancel"  class="btn btn-default btn-block" data-dismiss="modal">Cancel</button>
                                        </div>

                                        <div class="col-md-6">


                                                <a type="submit" class="btn btn-danger btn-block"
                                                   onclick="event.preventDefault();
                                                           document.getElementById('deleteItem{{$user->id}}').submit();">Confirm</a>

                                        </div>

                                    </div>

                                </div><!-- /.modal-content -->

                            </div><!-- /.modal-dialog -->

                        </div>


                    </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @foreach($users as $user)
        <form method="POST" id="deleteItem{{$user->id}}"  action="{{route('user.destroy',['id'=>$user->id])}}">
            @csrf
            @method('DELETE')
        </form>

        {{-- this is the edit modal--}}
        <div class="modal fade" :class="{ in: modalShown }" id="editModal{{$user->id}}"
             tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <form class="Edit-User-form" action="{{route('user.update',['id'=>$user->id])}}" method="post" data-count="{{$errors->count()}}" data-errors={{$errors}}>
                    @csrf
                    @method("PUT")
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title text-center" style="font-weight: bolder;" id="myModalLabel">Edit User</h3>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" class="form-control" name="id" id="id{{$user->id}}">
                            <div class="form-group">
                                <label for="" class="control-label {{$errors->has('name')? 'has-error':''}}">Name</label>
                                <input type="text" class="form-control" id="name{{$user->id}}" name="name">
                                @if($errors->has('name'))
                                    <span class="help-block">{{$errors->first('name')}}</span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('location')? 'has-error':''}}">
                                <label for="" class="control-label">location</label>
                                <select class="form-control" id="location{{$user->id}}" name="location">
                                </select>
                                @if($errors->has('location'))
                                    <span class="help-block">{{$errors->first('location')}}</span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('email')? 'has-error':''}}">
                                <label for="" class="control-label">Email</label>
                                <input type="text" class="form-control" id="email{{$user->id}}" name="email">
                                @if($errors->has('email'))
                                    <span class="help-block">{{$errors->first('email')}}</span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('phone')? 'has-error':''}}">
                                <label for="" class="control-label">Phone</label>
                                <input type="text" class="form-control" id="phone{{$user->id}}" name="phone">
                                @if($errors->has('phone'))
                                    <span class="help-block">{{$errors->first('phone')}}</span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('address')? 'has-error':''}}">
                                <label for="" class="control-label">Address</label>
                                <input type="text" name="address" id="address{{$user->id}}" class="form-control">
                                @if($errors->has('address'))
                                    <span class="help-block">{{$errors->first('address')}}</span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('user_type')? 'has-error':''}}">
                                <label class="control-label" for="">User Type</label>
                                <select class="form-control" name="user_type" id="">
                                    <option value="super_admin">Super Admin</option>
                                    <option value="collection_manager">Collections Manager</option>
                                    <option value="maxim_agent">Maxim Nyansa Agent</option>
                                </select>
                                @if($errors->has('user_type'))
                                    <span class="help-block">{{$errors->first('user_type')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button"  class="pull-left btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit"  class="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endforeach

@endsection


@push('scripts')
<script type="text/javascript">
    $('.admin-user-edit').on('click',function (e) {
        var id = $(this).data('id');
        $.ajax({
            type:'GET',
            url:'/admin/user/'+id,
            success:(data)=>{
                $('#id'+id).val(data.user.id);
                $('#name'+id).val(data.user.name);
                $('#email'+id).val(data.user.email);
                $('#phone'+id).val(data.user.phone);
                $('#address'+id).val(data.user.address);
                $.each(data.location,(key,value)=> {
                    $('#location'+id).append('<option value="'+value.id+'">'+value.name+'</option>');
                });

            },
            error:(error)=>{
                e.preventDefault();
                console.log(error);
            }
        });
        $('#editModal'+id).modal('show');
    });
</script>
@endpush