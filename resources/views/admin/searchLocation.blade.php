@extends('layouts.base')

@push('css')

@endpush


@section('title','Locations')


@section('content')
    <div class="container-fluid">
        <div class="row">
            <div style="padding-top: 20%;" class="col-md-6 col-md-offset-3">
                <form action="{{ route('admin.location.add') }}" method="post" id="Location-search">
                    @csrf
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search Location">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="submit">Search</button>
                         </span>
                    </div>
                </form>
                <span class="help-block">NB. Location Name,Country</span>
            </div>
        </div>
    </div>
@endsection


@push('scripts')

@endpush