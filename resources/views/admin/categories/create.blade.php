@extends('layouts.base')

@push('css')

@endpush


@section('title','Locations')


@section('content')
    @include('partials.alert')
    <div class="container-fluid" style="padding-bottom: 4rem;">
        <div class="top-flex-box">
            <p><span style="font-size: 4rem; " class="fa fa-list-alt"></span></p>
            <h2 style="font-weight: bold;">Add Category</h2>
        </div>

        <div class="row">
            <br>
            <br>
            <div class="row">
                <form method="post" action="{{route('categories.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-8 col-xs-10 col-md-offset-2 col-xs-offset-1" >

                                <div class="form-group {{$errors->has('name')? 'has-error':''}}">
                                    <label class="control-label" for="">Category Name</label>
                                    <input name="name" class="form-control" />
                                    @if($errors->has('name'))
                                        <span class="help-block">{{$errors->first('name')}}</span>
                                    @endif
                            </div>

                        <button type="submit" class="btn btn-primary pull-right">Add Item</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection


@push('scripts')

@endpush