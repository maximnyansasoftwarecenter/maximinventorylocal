@extends('layouts.base')

@push('css')

@endpush


@section('title','Items')


@section('content')
    @include('partials.alert')
    <div class="container-fluid" style="padding-bottom: 4rem;">
        <div class="top-flex-box">
            <p><span style="font-size: 4rem; " class="fa fa-dashcube"></span></p>
            <h2 style="font-weight: bold;">Add Item</h2>
        </div>

        <div class="row">
            <br>
            <br>
            <div class="row">
                <form method="post" action="{{route('items.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-8 col-xs-10 col-md-offset-2 col-xs-offset-1" >
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="control-label">Region</label>
                                    <select  class="form-control chosen-select"  name="region" id="" data-placeholder="Choose a Region...">
                                        <option value="ashanti">Ashanti</option>
                                        <option value="western">Western</option>
                                        <option value="greater_accra">Greater Accra</option>
                                        <option value="central">Central</option>
                                        <option value="eastern">Eastern</option>
                                        <option value="northern">Northern Region</option>
                                        <option value="upper_east">Upper East</option>
                                        <option value="upper_west">Upper West</option>
                                        <option value="volta">Volta</option>
                                        <option value="brong_ahafo">Brong-Ahafo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="" class="control-label ">District</label>
                                <select class="form-control chosen-select" name="district" id="">
                                    @foreach($districts as $district)
                                        <option value="{{$district->name}}">{{$district->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group {{$errors->has('school')? 'has-error':''}}">
                                    <label class="control-label " for="">School</label>
                                    <select name="school" class="form-control">
                                        <option></option>
                                    </select>
                                    @if($errors->has('school'))
                                        <span class="help-block">{{$errors->first('school')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group {{$errors->has('location')? 'has-error':''}}">
                                    <label class="control-label" for="">Location</label>
                                    <select class="form-control chosen-select" name="location">
                                        @foreach($locations as $location)
                                            <option value="{{$location->id}}">{{$location->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('category'))
                                        <span class="help-block">{{$errors->first('category')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group {{$errors->has('status')? 'has-error':''}}">
                                    <label class="control-label" for="">Status</label>
                                    <select name="status" class="form-control">
                                        <option value="active">Active</option>
                                        <option value="spoilt">Spoilt</option>
                                        <option value="maintenance">Maintenance</option>
                                        <option value="missing">Missing</option>
                                        <option value="recycle">Recycle</option>
                                    </select>
                                    @if($errors->has('status'))
                                        <span class="help-block">{{$errors->first('status')}}</span>
                                    @endif
                                </div>
                            </div>



                        </div>
                        <div class="row">
                            <input type="hidden" name="id" value="{{Auth::user()->id}}">
                            <div class="col-md-6">
                                <div class="form-group {{$errors->has('donor')? 'has-error':''}}">
                                    <label class="control-label " for="">Donor Name</label>
                                    <input class="form-control" type="text" name="donor" required>
                                    @if($errors->has('donor'))
                                        <span class="help-block">{{$errors->first('donor')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{$errors->has('category')? 'has-error':''}}">
                                    <label class="control-label" for="">Category</label>
                                    <select class="form-control chosen-select" name="category">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach

                                    </select>
                                    @if($errors->has('category'))
                                        <span class="help-block">{{$errors->first('category')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <dynamic-field></dynamic-field>

                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection


@push('scripts')
    <script type="text/javascript">
        $(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
    </script>
@endpush