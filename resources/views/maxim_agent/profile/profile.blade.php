@extends('maxim_agent.index')

@push('css')

@endpush


@section('title','Profile')


@section('content')
    @include('partials.alert')
    <div class="container-fluid">
        <div class="top-flex-box">
            <p><span style="font-size: 4.5rem; " class="fa fa-user-circle"></span></p>
            <h2 style="font-weight: bolder;">Your Profile</h2>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-1 col-xs-12"></div>
            <div class="col-lg-6 col-md-6 col-sm-10 col-xs-12">

                <form class="form" action="{{route('maxim_agent.profile.update')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="font-weight: bolder; text-align: center; font-size: 30px">
                                <span class="fa fa-plus"></span>
                                Enter Changes to Update
                            </h2>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <input type="hidden" name="id" value="{{Auth::user()->id }}">
                                <input type="hidden" name="image2" value="{{ Auth::user()->avatar }}">

                                <div class="form-group">
                                    <p style="text-align: center">
                                        <img src="{{ asset('images/locations/' . Auth::user()->avatar )}}" class="img img-responsive img-thumbnail" width="50%" height="50%">
                                    </p>
                                </div>

                                <div class="form-group {{$errors->has('name')? 'has-error':''}}">
                                    <label class="control-label" for="image">Change Image</label>
                                    <input id="image" class="form-control" type="file" name="image">
                                    @if($errors->has('image'))
                                        <span class="help-block">{{$errors->first('email')}}</span>
                                    @endif
                                </div>

                                <div class="form-group {{$errors->has('name')? 'has-error':''}}">
                                    <label class="control-label" for="name">Agent Name</label>
                                    <input id="name" class="form-control" type="text" name="name" value="{{ Auth::user()->name }}">
                                    @if($errors->has('name'))
                                        <span class="help-block">{{$errors->first('name')}}</span>
                                    @endif
                                </div>

                                <div class="form-group {{$errors->has('name')? 'has-error':''}}">
                                    <label class="control-label" for="email">User Email</label>
                                    <input id="email" class="form-control" type="email" name="email" value="{{ Auth::user()->email }}">
                                    @if($errors->has('email'))
                                        <span class="help-block">{{$errors->first('email')}}</span>
                                    @endif
                                </div>

                                <div class="form-group {{$errors->has('name')? 'has-error':''}}">
                                    <label class="control-label" for="phone">User Contact</label>
                                    <input id="phone" class="form-control" type="number" name="phone" value="{{ Auth::user()->phone }}">
                                    @if($errors->has('email'))
                                        <span class="help-block">{{$errors->first('phone')}}</span>
                                    @endif
                                </div>

                                <div class="form-group {{$errors->has('name')? 'has-error':''}}">
                                    <label class="control-label" for="address">User Address</label>
                                    <input id="phone" class="form-control" type="text" name="address" value="{{ Auth::user()->address }}">
                                    @if($errors->has('address'))
                                        <span class="help-block">{{$errors->first('address')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-secondary pull-right"> Save Changes</button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>


            </div>
            <div class="col-lg-3 col-md-3 col-sm-1 col-xs-12"></div>
        </div>
    </div>
@endsection


@push('scripts')

@endpush