
<div class="container-fluid">
    <div class="top-flex-box">
        <p><span style="font-size: 4.5rem; " class="fa fa-dashcube"></span></p>
        <h2 style="font-weight: bolder;">Items Report</h2>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <table class="datatable table table-bordered table-striped table-responsive">
                <thead>

                <tr>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Location</th>
                    <th>Category</th>
                </tr>

                </thead>
                <tbody>
                @foreach($today as $item)
                    <tr>
                        <td>{{$item->name}}</td>
                        <td>{{$item->quantity}}</td>
                        <td>{{$item->lname}}</td>
                        <td>{{$item->cname}}</td>
                    </tr>

                @endforeach
                </tbody>
            </table>
            <br>
            <br>
        </div>
    </div>
</div>