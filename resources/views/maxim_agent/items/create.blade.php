@extends('layouts.maximBase')

@push('css')

@endpush


@section('title','Items')


@section('content')
    @include('partials.alert')
    <div class="container-fluid" style="padding-bottom: 4rem;">
        <div class="top-flex-box">
            <p><span style="font-size: 4rem; " class="fa fa-dashcube"></span></p>
            <h2 style="font-weight: bold;">Add Item</h2>
        </div>

        <div class="row">
            <br>
            <br>
            <div class="row">
                <form method="post" action="{{route('maxim_agent_items.store')}}" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="{{Auth::user()->id}}">
                    @csrf
                    <div class="col-md-8 col-xs-10 col-md-offset-2 col-xs-offset-1" >
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{$errors->has('location')? 'has-error':''}}">
                                    <label class="control-label" for="">Location</label>
                                    <select class="form-control chosen-select" name="location">
                                        @foreach($locations as $location)
                                            <option value="{{$location->id}}">{{$location->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('category'))
                                        <span class="help-block">{{$errors->first('category')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{$errors->has('category')? 'has-error':''}}">
                                    <label class="control-label" for="">Category</label>
                                    <select class="form-control chosen-select" name="category">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach

                                    </select>
                                    @if($errors->has('category'))
                                        <span class="help-block">{{$errors->first('category')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{$errors->has('name')? 'has-error':''}}">
                                    <label class="control-label" for="">Item Name</label>
                                    <input type="text" class="form-control" name="name" required/>
                                    @if($errors->has('name'))
                                        <span class="help-block">{{$errors->first('name')}}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{$errors->has('quantity')? 'has-error':''}}">
                                    <label class="control-label" for="">Quantity</label>
                                    <input type="number" class="form-control" name="quantity"/>
                                    @if($errors->has('quantity'))
                                        <span class="help-block">{{$errors->first('quantity')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <textarea class="form-control" name="description" style="resize: none;"></textarea>
                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection


@push('scripts')
    <script type="text/javascript">
        $(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
    </script>
@endpush