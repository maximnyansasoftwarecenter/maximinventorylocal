<link rel="stylesheet" href="{{asset('dashboard/assets/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="/dashboard/assets/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/dashboard/assets/vendor/linearicons/style.css">
<link rel="stylesheet" href="{{asset('dashboard/assets/vendor/chartist/css/chartist-custom.css')}}">
<!-- MAIN CSS -->
<link rel="stylesheet" href="/dashboard/assets/css/main.css">
<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
<link rel="stylesheet" href="/dashboard/assets/css/demo.css">
<!-- GOOGLE FONTS -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
<!-- ICONS -->
<link rel="apple-touch-icon" sizes="76x76" href="/dashboard/assets/img/apple-icon.png">
<link rel="icon" type="image/png" sizes="96x96" href="/dashboard/assets/img/favicon.png">
<link rel="stylesheet" href="{{asset('datatables/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/css/admin.css')}}">
<link rel="stylesheet" href="{{asset('/css/chosen.min.css')}}">