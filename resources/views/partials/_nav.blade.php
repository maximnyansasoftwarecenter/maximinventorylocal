<div class="container-fluid" style="margin-bottom: 40px"> 
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">
    <span style="margin-left:10px;margin-top:-200px">
      <img src="{{ asset('images/c.png')}}" width="40px" height="50px"></span>
    <big style="color:rgb(178,212,85)">Computer<span style="font-size:50px;font-weight:bold">4</span>Schools</big>
  </a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar" aria-controls="myNavbar" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="myNavbar">
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12"></div>
    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">

      <form class="form my-2 my-lg-0" id="trackForm" method="post" action="{{route('donor.track')}}">
        <div  class="input-group form-group">
          <input id="match-error-pattern" class="form-control mr-sm-2" required
                 type="search" name="serial_number" placeholder="Search" aria-label="Search">
          <button @click="track" class="btn btn-outline-success my-2 my-sm-0" type="submit">TRACK</button>
        </div>
        @csrf
     </form>

    </div>
    
  </div>
</nav>

</div> <!--end of navigation -->