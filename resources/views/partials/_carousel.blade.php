<div class="container-fluid">
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="margin-top:-40px">
    <div class="carousel-inner">

        <div class="carousel-item active">
            <img class="img-responsive d-block w-100" src="{{ asset('images/cou12.jpg') }}" alt="First slide">
            <div class="container">
                <div class="carousel-caption">
                    <p  style="font-weight: bold;font-size:30px">Contribute today to create a better tomorrow</p>
                    <a><a data-toggle="modal" data-target="#LoginModal"  class="btn btn-outline-primary btn-lg">LOGIN</a></a>
                </div>
            </div>
        </div>

        <div class="carousel-item">
            <img class="img-responsive d-block w-100" src="{{ asset('images/cou2.jpg') }}" alt="Second slide">
            <div class="container">
                <div class="carousel-caption">
                    <p style="font-weight: bold;font-size:30px">Ghanain Students need your help.</p>
                    <p><a href="#" data-toggle="modal" data-target="#LoginModal" class="btn btn-outline-primary login-button btn-lg">LOGIN</a></p>
                </div>
            </div>
        </div>

        <div class="carousel-item">
            <img class="img-responsive d-block w-100" src="{{ asset('images/cou4.jpg') }}"  alt="Third slide">
            <div class="carousel-caption">
                <p  style="font-weight: bold;font-size:30px">Education is a necessity.</p>
                <p><a href="#" class="btn btn-outline-primary login-button btn-lg" data-toggle="modal" data-target="#LoginModal">LOGIN</a></p>
            </div>
        </div>

         <div class="carousel-item">
            <img class="img-responsive d-block w-100" src="{{ asset('images/cou1.jpg')}}" alt="Third slide">
            <div class="carousel-caption">
                <p  style="font-weight: bold;font-size:30px">Donate all your faulty Computers.</p>
                <p><a href="#" class="btn btn-outline-primary login-button btn-lg" data-toggle="modal" data-target="#LoginModal">LOGIN</a></p>
            </div>
        </div>

        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
             <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
    </div>

    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>

    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
</div>
<!--End of courosel-->