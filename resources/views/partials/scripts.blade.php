

<script src="{{asset('/dashboard/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('/dashboard/assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>

<script src="{{asset('/dashboard/assets/scripts/klorofil-common.js')}}"></script>
<script src="{{asset('datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('datatables/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/chosen.jquery.min.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>

<script>
    $('.custom-alerts').fadeOut(3000);
    $('.datatable').DataTable();
</script>