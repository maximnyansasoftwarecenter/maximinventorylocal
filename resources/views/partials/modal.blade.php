<div class="modal" id="confirm">
<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header" style="background-color: #2B333E;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title text-center ">Delete Confirmation</h4>
        </div>
        <div class="modal-body text-center">
            <p>Are you sure you, want to <b>DELETE</b> this user?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-xs btn-warning" id="delete-btn">Yes, Delete</button>
            <button type="button" class="btn btn-xs btn-info" data-dismiss="modal">No, Cancel</button>
        </div>
    </div>
</div>
</div>
