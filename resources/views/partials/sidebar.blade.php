<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <?php $segment = Request::segment(2);
                $maxim ='maxim';
                $collection = 'collection'
                ?>
                <li><a href="{{route('admin.dashboard')}}" class="{{ $segment == '' ? 'active' : ''  }}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
                    @if(Request::segment(1) == 'admin')
                <li><a href="{{ route('users')  }}" class="{{ $segment == 'users' ? 'active' : ''  }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
                @endif

                <li><a href="" class=""><i class="lnr lnr-alarm"></i> <span>Notifications</span></a></li>
                    <li><a href="{{route('regions.index')}}" class="{{ $segment == 'regions' ? 'active' : ''  }}"><i class="fa fa-globe"></i> <span>Regions & Districts</span></a></li>
                    <li><a href="{{route('schools.index')}}" class="{{ $segment == 'schools' ? 'active' : ''  }}">
                            <i class="fa fa-graduation-cap"></i> <span>Schools</span></a>
                    </li>
                    {{--computer 4 schools--}}
                    <li>

                        <a href="#computer" data-toggle="collapse" class="{{$segment == 'locations'||$segment == 'items'? 'active' : 'collapsed'}}"><i class="fa fa-navicon"></i> <span>Computer4Schools</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>

                        <div id="computer" class="collapse ">
                            <ul class="nav">
                                <li><a href="{{route('locations.index')}}" class="{{ $segment == 'locations' ? 'active' : ''  }}">
                                        <i class="fa fa-location-arrow"></i>
                                        Locations
                                    </a>
                                </li>
                                <li><a href="{{route('categories.index')}}" class="{{ $segment == 'categories' ? 'active' : ''}}">
                                        <i class="fa fa-list-alt"></i>
                                        Categories
                                    </a>
                                </li>

                                <li><a href="{{route('items.index')}}" class="{{ $segment == 'items' ? 'active' : ''  }}">
                                        <i class="fa fa-dashcube"></i>
                                        Items</a></li>

                            </ul>
                        </div>
                    </li>
                    {{--maxim inventory--}}
                    <li>

                        <a href="#maxim" data-toggle="collapse" class="{{$segment == 'mn_locations'||$segment == 'mn_items'||$segment=='mn_categories'? 'active' : ''}}"><i class="fa fa-navicon"></i> <span>Maxim Nyansa</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="maxim" class="collapse ">
                            <ul class="nav">
                                <li><a href="" class="{{ $segment == 'mn_locations' ? 'active' : ''  }}">

                                        <i class="fa fa-location-arrow"></i>
                                        Locations
                                    </a>
                                </li>
                                <li><a href="{{route('mn_categories.index')}}" class="{{ $segment == 'mn_categories' ? 'active' : ''  }}">
                                        <i class="fa fa-list-alt"></i>
                                        Categories
                                    </a>
                                </li>


                                <li><a href="{{route('mn_items.index')}}" class="{{ $segment == 'mn_items' ? 'active' : ''  }}">

                                        <i class="fa fa-dashcube"></i>
                                        Items</a></li>

                            </ul>
                        </div>
                    </li>


            </ul>
        </nav>
    </div>
</div>