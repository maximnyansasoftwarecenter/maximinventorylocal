
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta id="token" name="csrf-token" content="{{ csrf_token() }}">

    <title>Computer For School | @yield('title')</title>

    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

    @include('partials._nav')

    <div>
        @yield('content')
    </div>

    <div id="show-error-alert" class="alert alert-danger alert-dismissible custom-hide" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>Serial Number Invalid</strong>
    </div>

<script src="{{ asset('js/app.js') }}"></script>
    <script src="{{asset('js/chosen.jquery.min.js')}}"></script>
    @stack('scripts')

    <script type="text/javascript">
        $('#trackForm').submit(function (e) {
            $content = $('#trackForm input').val();

            if($content.length < 1){
                e.preventDefault();
                $('#trackForm input').addClass('is-invalid');
            }else{
                if(validate($content)){

                }else{
                    e.preventDefault();
                    var selector = $('#show-error-alert');
                        selector.toggleClass('custom-hide');
                }
            }
        });
            function validate(data){
                if(/^MN(\d)+\d$/i.test(data)){
                        return true;
                }else{
                   return false;
                }
            }
        $(function () {
            $('#trackForm input').focus(function () {
                if($(this).hasClass('is-invalid'))
                    $(this).removeClass('is-invalid');
            }) ;
        });

    </script>
</body>
</html>
