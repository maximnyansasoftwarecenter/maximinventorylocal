<!doctype html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- VENDOR CSS -->
    @include('partials.styles')
</head>

<body>


    <div id="wrapper">

    @include('partials.header')

    @include('partials.sidebar')

        <div class="main">

            <div id="app">
                @yield('content')
            </div>

        </div>
        <div class="clearfix"></div>
        <footer>
            <div class="container-fluid">
                <p class="copyright">&copy; 2017 <a href="https://www.themeineed.com" target="_blank">Theme I Need</a></p>
            </div>
        </footer>
    </div>

    <!-- END WRAPPER -->
    <!-- Javascript -->
    <script src="{{asset('/js/admin.js')}}"></script>
    <script src="{{asset('/dashboard/assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    @include('partials.scripts')
@stack('scripts')
</body>

</html>
