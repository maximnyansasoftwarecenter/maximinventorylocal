<!doctype html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    @include('partials.styles')
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
    <!-- NAVBAR -->
@include('partials.header')
<!-- END NAVBAR -->
    <!-- LEFT SIDEBAR -->
    <div id="sidebar-nav" class="sidebar">
        <div class="sidebar-scroll">
            <nav>
                <ul class="nav">
                    <?php $segment = Request::segment(2); ?>
                    <li><a href="" class="{{ $segment == '' ? 'active' : ''  }}"><i class="lnr lnr-home"></i> <span>Agent Dashboard</span></a></li>
                    <li><a href="{{route('maxim_agent_items.index')}}" class="{{ $segment == 'maxim_agent_items' ? 'active' : ''  }}"><i class="lnr lnr-chart-bars"></i> <span>Items</span></a></li>
                    <li><a href="{{route('maxim_agent_mn.reports')}}" class="{{ $segment == 'maxim_agent_reports' ? 'active' : ''  }}"><i class="fa fa-flag"></i> <span>Reports</span></a></li>

                    <li><a href="" class="{{ $segment == 'categories' ? 'active' : ''  }}">
                            <i class="fa fa-bell"></i>
                            Notifications
                        </a>
                    </li>
                    <li>
                        <a href="#subPages" data-toggle="collapse" class="{{ $segment == 'maxim_agent_profile' || $segment=='maxim_agent_password' ? 'active' : ''  }}  collapsed"><i class="fa fa-user"></i> <span>Profile</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                        <div id="subPages" class="collapse">
                            <ul class="nav">
                                <li><a href="{{route('maxim_agent.profile')}}" class="{{ $segment == 'maxim_agent_profile' ? 'active' : ''  }}"><i class="fa fa-eye"></i> View Profile</a></li>
                                <li><a href="{{route('maxim_agent.password')}}" class="{{ $segment == 'maxim_agent_password' ? 'active' : ''  }}"><i class="fa fa-plus"></i> Change Password</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
<!-- END LEFT SIDEBAR -->
    <!-- MAIN -->
    <div id="app">
        <div class="main">
        <!-- MAIN CONTENT -->
        @yield('content')
        <!-- END MAIN CONTENT -->
        </div>
    </div>
    <!-- END MAIN -->
    <div class="clearfix"></div>
    <footer>
        <div class="container-fluid">
            <p class="copyright">&copy; 2018 <a href="https://www.themeineed.com" target="_blank">Maxim Nyansa</a></p>
        </div>
    </footer>
</div>
<!-- END WRAPPER -->
<!-- Javascript -->

<script src="{{asset('/js/admin.js')}}"></script>
<script src="{{asset('/dashboard/assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
@include('partials.scripts')
@stack('scripts')
</body>

</html>
