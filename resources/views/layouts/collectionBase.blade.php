<!doctype html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- VENDOR CSS -->
    @include('partials.styles')
</head>

<body>
<div id="app">
    <!-- WRAPPER -->
    <div id="wrapper">
        <!-- NAVBAR -->
    @include('partials.header')
    <!-- END NAVBAR -->
        <!-- LEFT SIDEBAR -->
        <div id="sidebar-nav" class="sidebar">
            <div class="sidebar-scroll">
                <nav>
                    <ul class="nav">
                        <?php $segment = Request::segment(2); ?>
                        <li><a href="{{route('collections.dashboard')}}" class="{{ $segment == '' ? 'active' : ''  }}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>

                        <li><a href="{{route('computer_items.index')}}" class="{{ $segment == 'computer_items' ? 'active' : ''  }}">
                                <i class="fa fa-dashcube"></i>
                                Items</a></li>
                        <li><a href="{{route('collection.reports')}}"class="{{ $segment == 'reports' ? 'active' : ''  }}">
                                <i class="fa fa-flag"></i> <span>Reports</span></a>
                        </li>
                            <li><a href="{{route('items.move')}}" class="{{ $segment == 'move' ? 'active' : ''  }}"><i class="fa fa-globe"></i> <span>Move Items</span></a></li>
                            <li><a href="" class=""><i class="lnr lnr-alarm"></i> <span>Notifications</span></a></li>
                            <li>
                                <a href="#profile" data-toggle="collapse" class="collapsed {{ $segment == 'profile' || $segment == 'passwords' ? 'active' : ''  }}"><i class="fa fa-user-circle"></i> <span>Profile</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                                <div id="profile" class="collapse ">
                                    <ul class="nav">
                                        <li><a href="{{route('collection.profile')}}" class="{{ $segment == 'profile' ? 'active' : ''  }}">
                                                <i class="fa {{ $segment == 'profile' ? 'fa-eyedropper' : 'fa-eye'  }} ">
                                                </i> <span>View Profile</span></a>
                                        </li>
                                        <li><a href="{{route('collection.passwords')}}" class="{{ $segment == 'passwords' ? 'active' : ''  }}">
                                                <i class="fa fa-plus">
                                                </i> <span>Change Password</span></a>
                                        </li>

                                    </ul>
                                </div>
                            </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- END LEFT SIDEBAR -->
        <!-- MAIN -->
        <div class="main">
            <!-- MAIN CONTENT -->
        @yield('content')
        <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN -->
        <div class="clearfix"></div>
        <footer>
            <div class="container-fluid">
                <p class="copyright">&copy; 2017 <a href="https://www.themeineed.com" target="_blank">Theme I Need</a></p>
            </div>
        </footer>
    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->
</div>
<script src="{{asset('/js/admin.js')}}"></script>
<script src="{{asset('/dashboard/assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
@include('partials.scripts')
@stack('scripts')

</body>

</html>
