@extends('layouts.app')

@section('title','Home')

@section('content')
        <div class="container">
                <div class="row">
                        <div class="col-md-3">
                                <div>
                                        <img src="http://via.placeholder.com/300x200" alt="New" width="100%">
                                </div>

                        </div>
                        <div class="col-md-6">
                                <div>
                                        <table class="table table-bordered table-striped table-responsive">
                                                <thead>
                                                <tr>
                                                        <th>Item Name</th>
                                                        <th>Status</th>
                                                        <th>Location</th>
                                                        <th>School</th>
                                                        <th>Category</th>
                                                        <th>Quantity</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($items as $item)
                                                        <tr>
                                                                <td><strong>{{$item->name}}</strong></td>
                                                                <td>{{$item->status}}</td>
                                                                <td>{{$item->location_id}}</td>
                                                                <td>{{$item->school_id}}</td>
                                                                <td>{{$item->category_id}}</td>
                                                                <td>{{$item->quantity}}</td>
                                                        </tr>
                                                @endforeach
                                                </tbody>
                                        </table>
                                </div>

                        </div>
                        <div class="col-md-3">
                                <div>
                                        <div class="row">
                                                <img src="http://via.placeholder.com/300x200" alt="New" width="100%">
                                        </div>
                                        <hr>
                                        <div class="row">
                                                <img src="http://via.placeholder.com/300x200" alt="New" width="100%">
                                        </div>

                                </div>
                        </div>
                </div>
        </div>
@endsection

@push('scripts')

@endpush
