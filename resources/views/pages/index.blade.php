@extends('layouts.app')

@section('title','Home')

@section('content')
    @include('partials._carousel')
<div><br>
    <div class="container">
     <h2 style="font-weight: bolder;">Collection Points</h2>
      <hr>
        <div class="row locations-container">
            <div>
                <img src="images/loc1.jpg" width="200px" height="200px" class="img-circle">
                <div style="margin-left: 25px">
                    <span class="collection-headings">Location:</span> Nsuta
                </div>
                <div style="margin-left: 25px">
                    <img src="images/tel.png" height="25px" width="25px"> +233 54 323 5423
                </div>
                <div style="margin-left: 60px">
                    <a href="#" class="btn btn-success">Details</a>
                </div>
            </div>
            <div >
                <img src="images/loc2.jpg" width="200px" height="200px" class="img-circle">
                <div style="margin-left: 25px">
                    <span class="collection-headings">Location:</span> Ejisu
                </div>
                <div style="margin-left: 25px">
                    <img src="images/tel.png" height="25px" width="25px"> +233 21 323 5423
                </div>
                <div style="margin-left: 60px">
                    <a href="#" class="btn btn-success">Details</a>
                </div>
            </div>
            <div >
                <img src="images/loc3.jpg" width="200px" height="200px" class="img-circle">
                <div style="margin-left: 25px">
                    <span class="collection-headings">Location:</span> Kasoa
                </div>
                <div style="margin-left: 25px">
                    <img src="images/tel.png" height="25px" width="25px"> +233 26 323 5499
                </div>
                <div style="margin-left: 60px">
                    <a href="#" class="btn btn-success">Details</a>
                </div>
            </div>
            <div >
                <img src="images/loc4.jpg" width="200px" height="200px" class="img-circle">
                <div style="margin-left: 25px">
                    <span class="collection-headings">Location:</span> Kyebi
                </div>
                <div style="margin-left: 25px">
                    <img src="images/tel.png" height="25px" width="25px">+233 27 423 6723
                </div>
                <div style="margin-left: 60px">
                    <a href="#" class="btn btn-success">Details</a>
                </div>
            </div>
        </div>

    </div><br>
    <div style="background-color: rgb(178,212,85); height:5px"></div>
    <div style="background-color:orange; height:5px"></div><br>
    <div id="LoginApp">
        <login-component></login-component>
    </div>



</div>
@endsection

@push('scripts')

    @endpush
