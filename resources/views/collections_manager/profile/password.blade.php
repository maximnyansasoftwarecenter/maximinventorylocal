@extends('collections_manager.index')

@push('css')

@endpush


@section('title','Profile')


@section('content')
    @include('partials.alert')
    <div class="container-fluid">
        <div class="top-flex-box">
            <p><span style="font-size: 4.5rem; " class="fa fa-dashcube"></span></p>
            <h2 style="font-weight: bolder;">Change Password</h2>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-1 col-xs-12"></div>
            <div class="col-lg-6 col-md-6 col-sm-10 col-xs-12">

                <form class="form" action="{{route('collection.passwords.update')}}" method="post">
                    {{ csrf_field() }}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="font-weight: bolder; text-align: center; font-size: 30px">
                                <span class="fa fa-plus"></span>
                                Enter to Change Password
                            </h2>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <input type="hidden" name="hidden" value="{{Auth::user()->id }}">

                                <div class="form-group {{$errors->has('name')? 'has-error':''}}">
                                    <label class="control-label" for="name">Agent Name</label>
                                    <input id="name" class="form-control" type="text" name="name" value="{{ Auth::user()->name }}" disabled="disabled">
                                    @if($errors->has('name'))
                                        <span class="help-block">{{$errors->first('name')}}</span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="control-label" for="password">Enter New Password</label>
                                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                 <strong>{{ $errors->first('password') }}</strong>
                               </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label class="control-label" for="password-confirm">Confirm Password</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                                </div>

                            </div>
                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-secondary pull-right"> Save Changes</button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>


            </div>
            <div class="col-lg-3 col-md-3 col-sm-1 col-xs-12"></div>
        </div>
    </div>
@endsection


@push('scripts')

@endpush