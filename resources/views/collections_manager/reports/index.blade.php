@extends('layouts.collectionBase')

@push('css')

@endpush


@section('title','Add Item')


@section('content')
    @include('partials.alert')
    <div>
        <custom-item-tip></custom-item-tip>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#dayly" aria-controls="home" role="tab" data-toggle="tab">Today</a></li>
            <li role="presentation"><a href="#weekly" aria-controls="profile" role="tab" data-toggle="tab">This Week</a></li>
            <li role="presentation"><a href="#monthly" aria-controls="messages" role="tab" data-toggle="tab">This Month</a></li>
            <li role="presentation"><a href="#yearly" aria-controls="settings" role="tab" data-toggle="tab">This Year</a></li>
            <li id="custom-report-tip-section" role="presentation"><a href="#custom"  aria-controls="settings" role="tab" data-toggle="tab">Custom</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="dayly">
                @include('collections_manager.partials.today')
            </div>
            <div role="tabpanel" class="tab-pane" id="weekly">
                @include('collections_manager.partials.week')
            </div>
            <div role="tabpanel" class="tab-pane" id="monthly">
                @include('collections_manager.partials.month')
            </div>
            <div role="tabpanel" class="tab-pane" id="yearly">
                @include('collections_manager.partials.year')
            </div>


            <div role="tabpanel" class="tab-pane" id="custom">
                <div class="container-fluid">
                    <form  action="{{route('collection.reports.search')}}" method="post" id="custom-search-form">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="control-label">From</label>
                                    <input type="date" id="from" name="from" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="control-label">To</label>
                                    <input type="date" name="to" id="to" class="form-control">
                                </div>
                                <button id="custom-submit" class="btn btn-primary pull-right">Search</button>
                            </div>

                        </div>
                        @csrf
                    </form>
                    <br>

                    <div class="row" >
                        <div class="top-flex-box">
                            <p><span style="font-size: 4.5rem; " class="fa fa-dashcube"></span></p>
                            <h2 style="font-weight: bolder;">Items Report</h2>
                        </div>
                        <table id="datatable" class=" table table-bordered table-striped table-responsive" width="100%">
                            <thead>

                            <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Status</th>
                                <th>Donor</th>
                                <th>Location</th>
                                <th>School</th>
                                <th>Category</th>
                                <th>Delivered</th>
                            </tr>

                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>



@endsection


@push('scripts')
    <script>

        $(function () {
            $('#from').focus(function () {
                if($(this).parent('div').hasClass('has-error'))
                    $(this).parent('div').removeClass('has-error');
            })
            $('#to').focus(function () {
                if($(this).parent('div').hasClass('has-error'))
                    $(this).parent('div').removeClass('has-error');
            })
        });

        ///
        $('#myTabs a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });

        ///
        $('#custom-search-form').submit(function (e) {
            e.preventDefault();
           // $('#datatable')
            var from = $('#from');
            var to = $('#to');
            if(!from.val().length || !to.val().length){
                if(!from.val().length){
                    from.parent('div').addClass('has-error');
                }
                if(!to.val().length)
                    to.parent('div').addClass('has-error');
            }else{
                var url = $(this).attr('action');
                var data = $(this).serialize();
                $.ajax({
                    url: url,
                    type:"POST",
                    data:data,
                    success:function(data){
                        $('#datatable tbody').empty();
                        $('#datatable').dataTable({
                            data: data,
                            columns:[
                                {data : 'name'},
                                {data :'quantity'},
                                {data : 'status'},
                                {data : 'donor'},
                                {data : 'lname'},
                                {data : 'school_id'},
                                {data : 'cname'},
                                {data : 'delivered'}
                            ],
                        });
                    },
                    error:function (error) {
                        console.log(error);
                    }
                });
            }


        });

    </script>
@endpush