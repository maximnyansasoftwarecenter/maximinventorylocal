@extends('layouts.collectionBase')

@push('css')

@endpush


@section('title','Add Item')


@section('content')
    @include('partials.alert')
    <div class="container-fluid">
        <div class="top-flex-box">
            <p><span style="font-size: 4.5rem; " class="fa fa-dashcube"></span></p>
            <h2 style="font-weight: bolder;">Items</h2>
            <a href="{{ route('computer_items.create') }}" class="btn btn-primary icon__plus">Add Item <span class=" fa fa-plus"></span></a>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table class="datatable table table-bordered table-striped table-responsive">
                    <thead>

                    <tr>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Status</th>
                        <th>Donor</th>
                        <th>Location</th>
                        <th>School</th>
                        <th>Category</th>
                        <th>Delivered</th>
                        <th></th>
                    </tr>

                    </thead>
                    <tbody>
                    @foreach($data as $item)
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->quantity}}</td>
                            <td>{{$item->status}}</td>
                            <td>{{$item->donor}}</td>
                            <td>{{$item->lname}}</td>
                            <td>{{$item->sname}}</td>
                            <td>{{$item->cname}}</td>
                            @if($item->delivered == 1)
                                <td>Yes</td>
                                @else
                                <td>No</td>
                                @endif

                            <td style="display: flex; justify-content: space-evenly;">
                                <a type="button"  data-id="{{$item->id}}"  class="btn btn-sm btn-primary category-edit">
                                    <span class="fa fa-pencil"></span>
                                </a>
                                <button data-id="{{$item->id}}" type="button" data-toggle="modal" data-target="#deleteModal{{$item->id}}"
                                        class="btn btn-danger btn-sm">
                                    <span class="fa fa-trash"></span>
                                </button>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
                <br>
                <br>
            </div>
        </div>
    </div>

    @foreach($data as $item)
        {{--this is the delete modal--}}
        <div id="deleteModal{{$item->id}}" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title text-center" style="font-weight: bolder;">Are You Sure?</h3>
                    </div>
                    <div class="modal-footer row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-md-6">
                            <form  action="{{route('computer_items.destroy',['id'=>$item->id])}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-block">Confirm</button>
                            </form>
                        </div>

                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->

        </div>
        {{--edit modal--}}
        <div class="modal fade" :class="{ in: modalShown }" id="CategoryEditModal{{$item->id}}"
             tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <form class="Category-Edit" action="{{route('computer_items.update',['id'=>$item->id])}}" method="post" data-count="{{$errors->count()}}" data-errors={{$errors}}>
                    @csrf
                    @method("PUT")
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title text-center" style="font-weight: bolder;" id="myModalLabel">Edit Item</h3>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" class="form-control" name="id" value="{{$item->id}}">

                            <div class="form-group {{$errors->has('name')? 'has-error':''}}">
                                <label class="control-label" for="">Item Name</label>
                                <input id="name{{$item->id}}" class="form-control" type="text" name="name">
                                @if($errors->has('name'))
                                    <span class="help-block">{{$errors->first('name')}}</span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('status')? 'has-error':''}}">
                                <label class="control-label" for="">Status</label>
                                <select name="status" id="status{{$item->id}}" class="form-control">
                                    <option value="active">Active</option>
                                    <option value="spoilt">Spoilt</option>
                                    <option value="maintenance">Maintenance</option>
                                    <option value="missing">Missing</option>
                                    <option value="recycle">Recycle</option>
                                </select>
                                @if($errors->has('status'))
                                    <span class="help-block">{{$errors->first('status')}}</span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('donor')? 'has-error':''}}">
                                <label class="control-label " for="">Donor Name</label>
                                <input id="donor{{$item->id}}" class="form-control" type="text" name="donor">
                                @if($errors->has('donor'))
                                    <span class="help-block">{{$errors->first('donor')}}</span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('school')? 'has-error':''}}">
                                <label class="control-label " for="">School</label>
                                <select id="school{{$item->id}}" name="school" class="form-control">
                                    <option></option>
                                </select>
                                @if($errors->has('school'))
                                    <span class="help-block">{{$errors->first('school')}}</span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('category')? 'has-error':''}}">
                                <label class="control-label" for="">Category</label>
                                <select id="category{{$item->id}}" class="form-control" name="category">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('category'))
                                    <span class="help-block">{{$errors->first('category')}}</span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('location')? 'has-error':''}}">
                                <label class="control-label" for="">Location</label>
                                <select id="location{{$item->id}}" class="form-control" name="location">
                                    @foreach($locations as $location)
                                        <option value="{{$location->id}}">{{$location->name}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('category'))
                                    <span class="help-block">{{$errors->first('category')}}</span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('delivered')? 'has-error':''}}">
                                <label class="control-label" for="">Delivered?</label>
                                <select id="delivered{{$item->id}}" class="form-control" name="delivered" >
                                    <option value="0" {{$item->delivered == 0? 'selected':''}}>No</option>
                                    <option value="1" {{$item->delivered == 1? 'selected':''}}>Yes</option>
                                    <select>
                                        @if($errors->has('delivered'))
                                            <span class="help-block">{{$errors->first('delivered')}}</span>
                                @endif
                            </div>

                            <div class="form-group {{$errors->has('description')? 'has-error':''}}">
                                <label class="control-label" for="">Description</label>
                                <textarea id="description{{$item->id}}" class="form-control" type="file" name="description"></textarea>
                                @if($errors->has('image'))
                                    <span class="help-block">{{$errors->first('description')}}</span>
                                @endif
                            </div>
                            <input type="hidden" value="{{Auth::user()->id}}" name="id">
                        </div>

                        <div class="modal-footer">
                            <button type="button"  class="pull-left btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit"  class="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    @endforeach

@endsection


@push('scripts')
    <script type="text/javascript">
        var id;
        $('.category-edit').click(function(){
            id = $(this).data('id');
            //alert(id);
            //$('#delivered').html('');
            $.ajax({
                type:'GET',
                url:'/collections_manager/computer_items/'+id+'/edit',
                success:(data)=>{
                    //console.log(data);
                    $('#name'+id).val(data.name);
                    $('#description'+id).val(data.description);
                    $('#status'+id).val(data.status);
                    $('#donor'+id).val(data.donor);
                    $('#school'+id).val(data.school);
                },
                error:(error)=>{
                    e.preventDefault();
                    //console.log(error);
                }
            });
            $('#CategoryEditModal'+id).modal('show');
        });


    </script>
@endpush