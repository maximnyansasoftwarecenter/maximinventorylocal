
<div class="container-fluid">
    <div class="top-flex-box">
        <p><span style="font-size: 4.5rem; " class="fa fa-dashcube"></span></p>
        <h2 style="font-weight: bolder;">Items Report</h2>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <table class="datatable table table-bordered table-striped table-responsive">
                <thead>

                <tr>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Status</th>
                    <th>Donor</th>
                    <th>Location</th>
                    <th>School</th>
                    <th>Category</th>
                    <th>Delivered</th>
                </tr>

                </thead>
                <tbody>
                @foreach($today as $item)
                    <tr>
                        <td>{{$item->name}}</td>
                        <td>{{$item->quantity}}</td>
                        <td>{{$item->status}}</td>
                        <td>{{$item->donor}}</td>
                        <td>{{$item->lname}}</td>
                        <td>{{$item->sname}}</td>
                        <td>{{$item->cname}}</td>
                        @if($item->delivered == 1)
                            <td>Yes</td>
                        @else
                            <td>No</td>
                        @endif


                    </tr>

                @endforeach
                </tbody>
            </table>
            <br>
            <br>
        </div>
    </div>
</div>