try {
    window.$ = window.jQuery = require('jquery');
} catch (e) {}

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('custom-item-tip', require('./AdminComponents/CustomTip.vue'));
Vue.component('dynamic-field',require('./AdminComponents/DynamicForm.vue'));

const app = new Vue({
    el: '#app',
});
