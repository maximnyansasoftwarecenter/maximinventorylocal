<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->unsigned()->nullable(); // collect point 
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('avatar')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->enum('user_type',['super_admin','maxim_agent','collection_manager'])->default('maxim_agent');
            $table->rememberToken();
            $table->timestamps();


            $table->foreign('location_id')
                  ->references('id')
                  ->on('locations')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
