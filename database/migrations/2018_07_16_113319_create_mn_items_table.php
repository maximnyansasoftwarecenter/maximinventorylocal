<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMnItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mn_items', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('serial_number');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('name');
            $table->integer('mn_location_id')->unsigned()->nullable();
            $table->integer('mn_category_id')->unsigned()->nullable();
            $table->text('description')->nullable();
            $table->integer('quantity')->default('1');
            $table->date('purchase_date')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('mn_location_id')
                  ->references('id')
                  ->on('mn_locations')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

            $table->foreign('mn_category_id')
                  ->references('id')
                  ->on('mn_categories')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('set null')
                  ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mn_items');
    }
}
