<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MnItem extends Model
{

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function mn_location(){
        return $this->belongsTo(MnLocation::class);
    }
    public function mn_category(){
        return $this->belongsTo(MnCategory::class);
    }

    use SoftDeletes;

    protected $dates = ['deleted_at'];

}
