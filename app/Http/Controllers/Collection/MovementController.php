<?php

namespace App\Http\Controllers\Collection;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use Illuminate\Support\Facades\Auth;

class MovementController extends Controller
{
    public function index(){
       $data = Item::where('user_id',Auth::user()->id)->get();
       return view('collections_manager.movements.index',[
           'data'=>$data
       ]);
    }
}
