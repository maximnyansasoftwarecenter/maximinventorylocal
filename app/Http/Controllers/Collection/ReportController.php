<?php

namespace App\Http\Controllers\Collection;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Item;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class ReportController extends Controller
{

    public function reports(){
        $now = Carbon::now();
        $today = $now->toDateString();
        //daily report
        $data_today = DB::table('items')
            ->leftJoin('categories','items.category_id','=','categories.id')
            ->leftJoin('locations','items.location_id','=','locations.id')
            ->leftJoin('schools','items.school_id','=','schools.id')
            ->where('items.user_id','=',Auth::user()->id)
            ->whereDate('items.created_at',$today)
            ->select('items.*','locations.name as lname','categories.name as cname', 'schools.name as sname')
            ->orderby('items.created_at','desc')
            ->get();

        //weekly report
        $weekStart = $now->startOfWeek()->toDateString();
        $weekEnd = $now->endOfWeek()->toDateString();
        $data_weekly = DB::table('items')
            ->leftJoin('categories','items.category_id','=','categories.id')
            ->leftJoin('locations','items.location_id','=','locations.id')
            ->leftJoin('schools','items.school_id','=','schools.id')
            ->where('items.user_id','=',Auth::user()->id)
            ->whereBetween('items.created_at',[$weekStart,$weekEnd])
            ->select('items.*','locations.name as lname','categories.name as cname', 'schools.name as sname')
            ->orderby('items.created_at','desc')
            ->get();
        //monthly
        $month = $now->month;
        $data_monthly = DB::table('items')
            ->leftJoin('categories','items.category_id','=','categories.id')
            ->leftJoin('locations','items.location_id','=','locations.id')
            ->leftJoin('schools','items.school_id','=','schools.id')
            ->where('items.user_id','=',Auth::user()->id)
            ->whereMonth('items.created_at',$month)
            ->select('items.*','locations.name as lname','categories.name as cname', 'schools.name as sname')
            ->orderby('items.created_at','desc')
            ->get();
        //yearly
        $year = $now->year;
        $data_yearly = DB::table('items')
            ->leftJoin('categories','items.category_id','=','categories.id')
            ->leftJoin('locations','items.location_id','=','locations.id')
            ->leftJoin('schools','items.school_id','=','schools.id')
            ->where('items.user_id','=',Auth::user()->id)
            ->whereYear('items.created_at',$year)
            ->select('items.*','locations.name as lname','categories.name as cname', 'schools.name as sname')
            ->orderby('items.created_at','desc')
            ->get();

        return view('collections_manager.reports.index',[
            'today'=>$data_today,
            'weekly'=>$data_weekly,
            'monthly'=>$data_monthly,
            'yearly'=>$data_yearly,
        ]);
    }

    public function search(Request $request){
        if($request->ajax()){
            $start = $request->from;
            $end = $request->to;
            $data = DB::table('items')
                ->leftJoin('locations','items.location_id','=','locations.id')
                ->leftJoin('categories','items.category_id','=','categories.id')
                ->whereBetween('items.created_at',[$start,$end])
                ->select('items.*','locations.name as lname','categories.name as cname')
                ->get();
            return $data;

        }
    }
/*->join('schools','items.school_id','=','schools.id')

    //->whereBetween('items.created_at',[$start,$end])*/

}
