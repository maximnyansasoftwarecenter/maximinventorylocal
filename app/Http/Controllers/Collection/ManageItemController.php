<?php

namespace App\Http\Controllers\Collection;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Location;
use App\Item;
use App\Category;
use Illuminate\Support\Facades\DB;
use App\GeneralSerial;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ManageItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $locations = Location::all();

        $source = DB::table('items')
            ->leftJoin('categories','items.category_id','=','categories.id')
            ->leftJoin('locations','items.location_id','=','locations.id')
            ->leftJoin('schools','items.school_id','=','schools.id')
            ->where('items.user_id','=',Auth::user()->id)
            ->select('items.*','locations.name as lname','categories.name as cname', 'schools.name as sname')
            ->get();

        return view('collections_manager.items.index',[
            'data'=>$source,
            'locations'=>$locations,
            'categories'=>$categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = file_get_contents(public_path('files/districts.json'));
        $categories = Category::all();
        $locations = Location::all();
        return view('collections_manager.items.create',[
            'locations'=>$locations,
            'categories'=>$categories,
            'districts'=>json_decode($data)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $this->validate($request, [
            'name' => 'required',
            'donor' => 'required',
        ]);
        //generating the general id
        // $test = DB::table('general_serials')->pluck('serial_number');

        $data = GeneralSerial::all()->count();
        $base = "MN";
        $num = $data + 1;
        switch ($num) {
            case  $num < 10 :
                $base = $base . '0000' . $num;
                break;
            case $num < 100:
                $base = $base . '000' . $num;
                break;
            case $num < 1000:
                $base = $base . '00' . $num;
                break;
            case $num < 10000:
                $base = $base . '0' . $num;
                break;
            default:
                $base = $base . '' . $num;
                break;
        }
        $general_id = new GeneralSerial();
        $general_id->serial_number = $base;
        $general_id->save();
        //get general id
        $test = DB::table('general_serials')->orderby('created_at','DESC')->first();
        if (count($request['quantity']) > 1) {
            for ($i = 0; $i < count($request['quantity']); $i++) {


                $item = new Item();
                $item->region = $request['region'];
                $item->district = $request['district'];
                $item->name = $request['name'][$i];
                $item->quantity =  $request['quantity'][$i];
                $item->user_id = $request->id;
                $item->donor = $request->donor;

                $item->serial_number = (string)Str::uuid();
                $item->general_serial_id = $test->id;


                $item->status = $request->status;
                $item->location_id = $request->location;
                if ($request->has('description'))
                    $item->description = $request->description;
                if ($request->has('category'))
                    $item->category_id = $request->category;
                $item->save();
            }

            return redirect()->route('computer_items.index')->with([
                'success' => 'Added Successfully',
            ]);


        }



        //dd($request->name[0]);

        $item = new Item();
        $item->name = $request['name'][0];
        $item->quantity = 1;
        $item->user_id = $request->id;
        $item->donor = $request->donor;

        $item->serial_number = (string)Str::uuid();
        $item->general_serial_id = $test->id;

        $item->region = $request['region'];
        $item->district = $request['district'];
        $item->status = $request->status;
        $item->location_id = $request->location;
        if ($request->quantity[0] == null)
            $item->quantity = 1;
        else
            $item->quantity = $request->quantity[0];
        if ($request->has('description'))
            $item->description = $request->description;
        if ($request->has('category'))
            $item->category_id = $request->category;
        if ($item->save()) {
            return redirect()->route('computer_items.index')->with([
                'success' => 'Added Successfully',
            ]);
        } else {
            return redirect()->back()->with([
                'error' => 'Failed',
            ]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        return $item;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('items')->where('id',$id)
            ->update(
                [
                    'name' => $request->name,
                    'description' => $request->description,
                    'status' => $request->status,
                    'category_id' => $request->category,
                    'location_id' => $request->location,
                    'school_id' => $request->school,
                    'delivered' => $request->delivered,
                ]);


        return redirect()->back()->with([
            'success' => 'Edited Successfully!',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        $item->delete();
        return redirect()->back()->with([
            'success' =>'deleted Successfully'
        ]);
    }
}
