<?php

namespace App\Http\Controllers\Collection;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
class ProfileController extends Controller
{

    public function update(Request $request){
        $this->validate($request,[
            'name'=>'required|string',
            'email'=>'unique:users,email,'.$request->id,
        ]);


        $id = User::find($request['id']);

        $id->name = $request->name;
        $id->phone = $request->phone;
        $id->email = $request->email;
        $id->address = $request->address;

        if($request->hasFile('image')){
            $locationImage  = $request->file('image');
            $filename = time().$locationImage->getClientOriginalName();
            Image::make($locationImage)->resize(300,300)->save( public_path('images/locations/' . $filename));

        }else {

            $filename = $request->image2;;

        }
        $id->avatar = $filename;

        if($id->save()){

            return redirect()->back()->with(['success' => 'Updated Successfully',]);

        }else{

            return redirect()->back()->with(['error' => 'Updated Failed',]);
        }
    }

    public function password(Request $request){
        $this->validate($request,[
            'password' => 'required|confirmed|min:6',
        ]);
        $pass = User::find($request['hidden']);
        $pass->password = bcrypt($request["password"]);;
        if($pass->save()){
            return redirect()->back()->with(['success' => 'Password Changed Successfully',]);
        }else{
            return redirect()->back()->with(['error' => 'Failed!',]);
        }
    }
}
