<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Location;
use Illuminate\Support\Facades\DB;
use GMaps;
use Image;
class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $location = Location::all();

        return view('admin.location',['locationsData'=>$location]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'string',
            //'image'=>'image'
        ]);

        $location = new Location();
        $location->name = $request->name;

            $id = $request->location;
            $data = DB::table('gmaps_geocache')->where('id',$id)->first();
            $location->latitude = $data->latitude;
            $location->longitude = $data->longitude;
        // if($request->has('opening_hours'))
            $location->opening_hours = $request->opening_hours;

        if($request->hasFile('image')){
            $locationImage  = $request->file('image');
            $filename = time().$locationImage->getClientOriginalName();
            Image::make($locationImage)->resize(200,200)->save( public_path('images/locations/' . $filename));
            $location->avatar=$filename;
        }else {
            $location->avatar = 'avatar.png';
        }
        if($location->save()){
            return redirect()->route('locations.index')->with([
                'success' => 'Added Successfully',
            ]);
        }else{
            return redirect()->route('locations.index')->with([
                'error' => 'Failed',
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::find($id);
        $location->delete();
        return redirect()->back()->with([
            'success' =>'deleted Successfully'
        ]);
    }
}
