<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
//use FarhanWazir\GoogleMaps\GMaps;
use FarhanWazir\GoogleMaps\Facades\GMapsFacade as GMaps;


class MiscController extends Controller
{
    //c4s functions

    public function show(){
        return view('admin.searchLocation');
    }

    public function search(Request $request){
        $config['center'] = $request->search;
        $config['zoom'] = 15;
        $config['scroll_wheel']=false;
        $config['geocodeCaching']=true;

        GMaps::initialize($config);

        $marker['position']= $request->search;
        $marker['infowindow_content'] = $request->search;
        GMaps::add_marker($marker);
        $map = GMaps::create_map();

        $location = DB::table('gmaps_geocache')->get();
        return view('admin.addLocation',['maps'=>$map,'locations'=>$location]);
    }


// Maxim nyansa functions

    public function mn_locationShow(){
        return view('maxim_agent.mn_locationsearch');
    }


    public function mn_search(Request $request){

        $config['center'] = $request->mn_search;
        $config['zoom'] = 15;
        $config['scroll_wheel']=false;
        $config['geocodeCaching']=true;

        GMaps::initialize($config);

        $marker['position']= $request->mn_search;
        $marker['infowindow_content'] = $request->mn_search;
        GMaps::add_marker($marker);

         $map = GMaps::create_map();
         $mn_location = DB::table('gmaps_geocache')->get();
         return view('maxim_agent.mn_addLocation', ['map'=>$map,'mn_locations'=>$mn_location]);
        
    }

}
