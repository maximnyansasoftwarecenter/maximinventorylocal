<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
class LoginController extends Controller
{
    public function login(Request $request){
        $url = '';
        if($request->ajax()){
            if(Auth::attempt(['email'=>$request['email'],'password'=>$request['password']],$request->remember)){
                $user = User::where('email',$request->email)->pluck('user_type');
                if($user[0] == 'super_admin'){
                    //  return redirect()->route('admin.dashboard');
                    $url.='admin';
                }
                elseif ($user[0] == 'maxim_agent'){
                    $url.='maxim_agent';
                }
                else{
                    $url.='collections_manager';
                }
            }else {
                $url = null;
            }
            return $url;
        }


    }
}
