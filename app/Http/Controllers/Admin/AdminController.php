<?php

namespace App\Http\Controllers\Admin;

use App\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //dd($request);
        $this->validate($request,[
            'name'=>'required',
            'phone'=>'digits_between:10,14',
            'email'=>'email',
            'location'=>'required',
            'picture'=>'image'

        ]);
        $user = new User();
        $user->name = $request->name;
        $user->password = bcrypt("password");
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->location_id = $request->location;
        $user->user_type = $request->user_type;

         if($request->hasFile('picture')){
            $locationImage  = $request->file('picture');
            $filename = time().$locationImage->getClientOriginalName();
            Image::make($locationImage)->resize(300,300)->save( public_path('images/locations/' . $filename));
             $user->avatar=$filename;
        }else {
             $user->avatar = 'avatar.png';
         }
         
        if($user->save()){
            return redirect()->route('users')->with([
                'success' => 'Added Successfully',
            ]);
        }else{
            return redirect()->back()->with([
                'error' => 'Failed',
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $location = Location::all();
        return response()->json([
            'user' =>$user,
            'location'=>$location,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required|string',
            'phone'=>'digits_between:10,14',
            'email'=>'unique:users,email,'.$request->id,
            'location'=>'required',
        ]);
        DB::table('users')->where('id',$id)
            ->update(
            [
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'address' => $request->address,
                'location_id' => $request->location,
                'user_type' => $request->user_type,
        ]);


            return redirect()->back()->with([
                'success' => 'Edited Successfully!',
            ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->with([
            'success' =>'deleted Successfully'
        ]);
    }
}
