<?php

namespace App\Http\Controllers\Maxim;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class MnReportController extends Controller
{
    public function reports(){
        $now = Carbon::now();
        $today = $now->toDateString();
        //daily report
        $data_today = DB::table('mn_items')
            ->leftJoin('mn_categories','mn_items.mn_category_id','=','mn_categories.id')
            ->leftJoin('mn_locations','mn_items.mn_location_id','=','mn_locations.id')
            ->where('mn_items.user_id','=',Auth::user()->id)
            ->whereDate('mn_items.created_at',$today)
            ->select('mn_items.*','mn_locations.name as lname','mn_categories.name as cname')
            ->orderby('mn_items.created_at','desc')
            ->get();

        //weekly report
        $weekStart = $now->startOfWeek()->toDateString();
        $weekEnd = $now->endOfWeek()->toDateString();
        $data_weekly = DB::table('mn_items')
            ->leftJoin('mn_categories','mn_items.mn_category_id','=','mn_categories.id')
            ->leftJoin('mn_locations','mn_items.mn_location_id','=','mn_locations.id')
            ->where('mn_items.user_id','=',Auth::user()->id)
            ->whereBetween('mn_items.created_at',[$weekStart,$weekEnd])
            ->select('mn_items.*','mn_locations.name as lname','mn_categories.name as cname')
            ->orderby('mn_items.created_at','desc')
            ->get();
        //monthly
        $month = $now->month;
        $data_monthly = DB::table('mn_items')
            ->leftJoin('mn_categories','mn_items.mn_category_id','=','mn_categories.id')
            ->leftJoin('mn_locations','mn_items.mn_location_id','=','mn_locations.id')
            ->where('mn_items.user_id','=',Auth::user()->id)
            ->whereMonth('mn_items.created_at',$month)
            ->select('mn_items.*','mn_locations.name as lname','mn_categories.name as cname')
            ->orderby('mn_items.created_at','desc')
            ->get();
        //yearly
        $year = $now->year;
        $data_yearly = DB::table('mn_items')
            ->leftJoin('mn_categories','mn_items.mn_category_id','=','mn_categories.id')
            ->leftJoin('mn_locations','mn_items.mn_location_id','=','mn_locations.id')
            ->where('mn_items.user_id','=',Auth::user()->id)
            ->whereYear('mn_items.created_at',$year)
            ->select('mn_items.*','mn_locations.name as lname','mn_categories.name as cname')
            ->orderby('mn_items.created_at','desc')
            ->get();

        return view('maxim_agent.reports.index',[
            'today'=>$data_today,
            'weekly'=>$data_weekly,
            'monthly'=>$data_monthly,
            'yearly'=>$data_yearly,
        ]);
    }

    public function search(Request $request){
        if($request->ajax()){
            $start = $request->from;
            $end = $request->to;
            $data = DB::table('mn_items')
                ->leftJoin('mn_locations','mn_items.mn_location_id','=','mn_locations.id')
                ->leftJoin('mn_categories','mn_items.mn_category_id','=','mn_categories.id')
                ->whereBetween('mn_items.created_at',[$start,$end])
                ->select('mn_items.*','mn_locations.name as lname','mn_categories.name as cname')
                ->get();
            return $data;

        }
    }
}
