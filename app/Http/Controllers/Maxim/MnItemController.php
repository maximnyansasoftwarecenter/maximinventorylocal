<?php

namespace App\Http\Controllers\Maxim;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MnLocation;
use App\MnCategory;
use App\MnItem;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
class MnItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = MnCategory::all();
        $locations = MnLocation::all();
        $items = MnItem::where('user_id',Auth::user()->id)->get();
        return view('maxim_agent.items.index',
            [
                'data'=>$items,
                'categories'=>$categories,
                'locations'=>$locations
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = file_get_contents(public_path('files/districts.json'));
        $categories = MnCategory::all();
        $locations = MnLocation::all();
        return view('maxim_agent.items.create',[
            'locations'=>$locations,
            'categories'=>$categories,
            'districts'=>json_decode($data)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $this->validate($request, [
            'name' => 'required',
        ]);
        //generating the general id
        // $test = DB::table('general_serials')->pluck('serial_number');

        $data = MnItem::all()->count();
        $base = "MN";
        $num = $data + 1;
        switch ($num) {
            case  $num < 10 :
                $base = $base . '0000' . $num;
                break;
            case $num < 100:
                $base = $base . '000' . $num;
                break;
            case $num < 1000:
                $base = $base . '00' . $num;
                break;
            case $num < 10000:
                $base = $base . '0' . $num;
                break;
            default:
                $base = $base . '' . $num;
                break;
        }

        //dd($request->name[0]);

        $item = new MnItem();
        $item->name = $request['name'];
        $item->user_id = $request->id;
        $item->serial_number = $base;
        $item->mn_location_id = $request->location;
        if ($request->quantity == null)
            $item->quantity = 1;
        else
            $item->quantity = $request->quantity;
        if ($request->has('description'))
            $item->description = $request->description;
        if ($request->has('category'))
            $item->mn_category_id = $request->category;

        if ($item->save()) {
            return redirect()->route('maxim_agent_items.index')->with([
                'success' => 'Added Successfully',
            ]);
        } else {
            return redirect()->back()->with([
                'error' => 'Failed',
            ]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = MnItem::find($id);
        return $item;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('mn_items')->where('id',$id)
            ->update(
                [
                    'name' => $request->name,
                    'description' => $request->description,
                    'mn_category_id' => $request->category,
                    'mn_location_id' => $request->location,
                ]);


        return redirect()->back()->with([
            'success' => 'Edited Successfully!',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = MnItem::find($id);
        $item->delete();
        return redirect()->back()->with([
            'success' =>'deleted Successfully'
        ]);
    }
}
