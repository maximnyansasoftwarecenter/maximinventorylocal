<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GeneralSerial;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   /* public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    protected $newData;
    public function index()
    {
        return view('home');
    }
    public function track(Request $request){
        $search = GeneralSerial::where('serial_number',$request->serial_number)->first();
        if(count($search) > 0){
            //$newData = $send = Hash::make($search->serial_number);
            return redirect()->route('donor.track.details',['detail'=>$search->serial_number]);
        }
        else{
            return redirect()->back();
        }
    }
    public function details($detail){
        $search = GeneralSerial::where('serial_number',$detail)->first();
       // dd($search);
        $req = DB::table('items')->where('general_serial_id',$search->id)->get();
        //dd($req);
        return view('pages.track',['items'=>$req]);
    }
}
