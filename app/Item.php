<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Item extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function location(){
        return $this->belongsTo('App\Location');
    }
    public function category(){
        return $this->belongsTo('App\Category');
    }
    public function school(){
        return $this->belongsTo('App\School');
    }
}
