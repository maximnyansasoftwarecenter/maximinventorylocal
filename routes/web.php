<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.index');
});



Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/loginUser','Admin\LoginController@login')->name('user.login');

//tracking routes
Route::post('/track','HomeController@track')->name('donor.track');
Route::get('/track/details/{detail}','HomeController@details')->name('donor.track.details');







