<?php

use App\Location;
use App\User;
use Illuminate\Http\Request;


Route::get('/',function(){
    return view('admin.index');
})->name('admin.dashboard');

Route::get('/users',function(){
    $user = User::all();
    return view('admin.users',['users'=>$user]);
})->name('users');

Route::get('/users/add',function (){

    $location = Location::all();
   return view('admin.addUser',['locations' => $location]);
})->name('admin.users.add');


Route::get('/locations/add/search','Admin\MiscController@show')->name('admin.location.search');
Route::post('/locations/add/search/data','Admin\MiscController@search')->name('admin.location.add');

Route::resources([
    'user' => 'Admin\AdminController',
    'locations'=>'Admin\LocationController',
    'items'=>'Admin\ItemController',
    'categories'=>'Admin\CategoryController',
    'schools'=>'Admin\SchoolController',
    'mn_items'=>'Admin\MnItemsController',
    'regions'=>'Admin\RegionController',
    'mn_categories'=>'Admin\MnCategoryController'
]);











