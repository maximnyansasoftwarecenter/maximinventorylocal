<?php

Route::get('/',function(){
   return view('collections_manager.index');
})->name('collections.dashboard');
//profile routes
Route::view('profile','collections_manager.profile.profile')->name('collection.profile');
Route::post('profile/update','Collection\ProfileController@update')->name('collection.update.profile');
//report routes
Route::get('reports','Collection\ReportController@reports')->name('collection.reports');
Route::post('reports/custom_search','Collection\ReportController@search')->name('collection.reports.search');
//password routes
Route::view('passwords','collections_manager.profile.password')->name('collection.passwords');
Route::post('passwords/update','Collection\ProfileController@password')->name('collection.passwords.update');

//movements routes
Route::get('/move','Collection\MovementController@index')->name('items.move');


Route::resources([
    /*'user' => 'Admin\AdminController',
    'locations'=>'Admin\LocationController',*/
    'computer_items'=>'Collection\ManageItemController',
    /*'categories'=>'Admin\CategoryController',
    'schools'=>'Admin\SchoolController',
    'mn_items'=>'Admin\MnItemsController'*/
]);