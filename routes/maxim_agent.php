<?php
Route::get('/',function(){
    return view('maxim_agent.index');
})->name('maxim.dashboard');

Route::get('/maxim_agent_reports','Maxim\MnReportController@reports')->name('maxim_agent_mn.reports');
Route::post('/maxim_agent_reports/search','Maxim\MnReportController@search')->name('maxim_agent_mn.reports.search');

Route::get('/maxim_agent_profile',function (){
    return view('maxim_agent.profile.profile');
})->name('maxim_agent.profile');

Route::post('/maxim_agent_profile_update','Maxim\MnProfileController@update')->name('maxim_agent.profile.update');
//maxim_agent_password
Route::get('/maxim_agent_password',function (){
    return view('maxim_agent.profile.password');
})->name('maxim_agent.password');
//password
Route::post('/maxim_agent_password_update','Maxim\MnProfileController@password')->name('maxim_agent.password.update');


//resource routes
Route::resources([
    'maxim_agent_items'=>'Maxim\MnItemController',
]);